import Swal from 'sweetalert2'

export function messageErrorToast(error) {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });
    Toast.fire({
      type: 'error',
      title: error
    })
}
export function messageSuccess(msj) {
  Swal.fire({
    type: 'success',
    title: msj,
    showConfirmButton: false,
    timer: 2000    
  })
}

export function messageError(msj) {
  Swal.fire({
    type: 'error',
    title: msj,
    showConfirmButton: false,
    timer: 2000     
  })
}

export function deleteTask() {
}