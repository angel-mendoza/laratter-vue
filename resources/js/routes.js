import Home from './components/sections-app/Home.vue'
import Dashboard from './components/dashboard/Dashboard.vue'

export const routes = [
	{
		path: '/',
		component: Home
	},
	{
		path: '/dashboard',
		component: Dashboard,
		meta: {
			requiresAuth: true
		}			
	}

]