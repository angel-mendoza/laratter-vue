require('./bootstrap');
import Vue from 'vue'
import VueSweetalert2 from 'vue-sweetalert2'
import VueRouter from 'vue-router'
import {initialize} from './general';
import Vuex from 'vuex'

import { routes } from './routes'
import StoreData from './store'

import MainApp from './components/sections-app/MainApp.vue'

Vue.use(VueSweetalert2, options)
Vue.use(VueRouter)
Vue.use(Vuex)

const options = {
	confirmButtonColor: '#41b882',
	cancelButtonColor: '#ff7674'
}

const store = new Vuex.Store(StoreData)


const router = new VueRouter({
    routes,
    mode: 'history'
})

initialize(store, router)

const app = new Vue({
    el: '#app',
    router,
    store,
    components: {
    	MainApp
    }
});