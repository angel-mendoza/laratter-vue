import { getLocalUser } from "./helpers/auth";
import { setAuthorization } from "./general";

const user = getLocalUser();

export default {
    state: {
        nameApp: "Laratter",
        currentUser: user,
        isLoggedIn: !!user,
        loading: false,
        tasks: []
    },
    getters: {
        getNameApp(state) {
            return state.nameApp
        },
        getCurrentUser(state) {
            return state.currentUser
        },
        isLoading(state) {
            return state.loading
        },
        isLoggedIn(state) {
            return state.isLoggedIn
        },
        getTasks(state){
            return state.tasks
        }
    },
    mutations: {
        login(state) {
            state.loading = true
        },
        register(state) {
            state.loading = true
        },        
        loginSuccess(state, payload) {
            state.isLoggedIn = true
            state.loading = false
            state.currentUser = Object.assign({}, payload.user, {token: payload.access_token})
            localStorage.setItem("user", JSON.stringify(state.currentUser))
        },
        loginFailed(state, payload) {
            state.loading = false;
            state.isLoggedIn = false
        },
        logout(state) {
            localStorage.removeItem("user");
            state.isLoggedIn = false;
            state.currentUser = null;
        },
        registerSuccess(state, payload) {
            state.isLoggedIn = true;
            state.loading = false;
            state.currentUser = Object.assign({}, payload.user, {token: payload.access_token});
            localStorage.setItem("user", JSON.stringify(state.currentUser));
        },
        registerFailed(state, payload){
            state.loading = false;
            state.isLoggedIn = false
        },
        getTasks(state , payload){
            state.tasks = payload
        }        
    },
    actions: {
        login(context , payload){
            return new Promise((res,rej)=>{
                axios.post('/api/auth/login', payload)
                    .then((response) =>{
                        setAuthorization(response.data.access_token)
                        res(response.data)
                    })
                    .catch((err) =>{
                        rej("Error en el Email o La Contraseña");
                    })                    
            })
        },
        register(context , payload) {
            return new Promise((res, rej) => {
                axios.post('/api/auth/register', payload)
                    .then((response) => {
                        setAuthorization(response.data.access_token)
                        res(response.data)

                    })
                    .catch((err) =>{
                        rej("Algo salio mal, intentelo mas tarde!");
                    })
            })
        },
        getTasks(context){
            axios.get('/api/tasks', {
                headers:{
                    "Authorization": `Bearer ${context.state.currentUser.token}`
                }
            })
            .then( (response) => {
                context.commit('getTasks', response.data.tasks)
            })
        },
        createTask(context , payload) {
            return new Promise((res, rej) => {
                axios.post('/api/tasks/new', payload)
                    .then((response) => {
                        setAuthorization(response.data.access_token)
                        res(response.data)

                    })
                    .catch((err) =>{
                        rej("Algo salio mal, intentelo mas tarde!");
                    })
            })
        },        
        deleteTask(context , payload) {
            return new Promise((res, rej) => {
                axios.delete(`/api/tasks/deleteTask/${payload}`)
                    .then((response) => {
                        setAuthorization(response.data.access_token)
                        res(response.data)

                    })
                    .catch((err) =>{
                        console.log(payload)
                        rej("Algo salio mal, intentelo mas tarde!");
                    })
            })
        } 
    }
};


