<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function all()
    {
       $tasks = Task::all();

        return response()->json([
            "tasks" => $tasks
        ], 200);
    }

    public function new(Request $request)
    {

        $credentials = request(['name' ,'description']);

        $task = new Task();
        $task->name = $credentials['name'];
        $task->description = $credentials['description'];
        $task->save();

        return response()->json([
            "task" => $task
        ], 200);
    }

    public function delete($id)
    {
        $taks = Task::findOrFail($id);
        $taks->delete();

        return response()->json([
            "msj" => "successfully deleted task"
        ], 200);
    }    
}
