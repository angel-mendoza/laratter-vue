<?php

use Faker\Generator as Faker;

$factory->define(App\Task::class, function (Faker $faker) {
    return [
 		'name' => $faker->jobTitle,
        'description' => $faker->text($maxNbChars = 70),
        'status' => false
    ];
});
